class Box:
    def __init__(self):
        self._contents = []
        self._open = True
        self._capacity = None

    def __contains__(self, machin):
        return machin in self._contents

    def add(self, truc):
        self._contents.append(truc)

    def remove(self, truc2):
        self._contents.remove(truc2)

    def is_open(self):
        return self._open

    def open(self):
        self._open = True

    def close(self):
        self._open=False

    def action_look(self):
        if not self.is_open():
            return "la boite est fermée"
        else:
            return "la boite contient "+', '.join(self._contents)

    def set_capacity(self, capacity):
        self._capacity = capacity

    def capacity(self):
        return self._capacity

    def has_room_for(self, truc):
        return self._capacity is None or self._capacity >= truc.volume()

    def action_add(self, truc):
        if self.is_open() and self.has_room_for(truc):
            self.add(truc)
            if self.capacity != None:
                self._capacity -= truc._volume
            return True
        else:
            return False


class Thing:
    def __init__(self, vol=None, name=None):
        self._volume = vol
        self._name = name

    def volume(self):
        return self._volume

    def set_name(self, name):
        self._name = name

    def has_name(self, name):
        if self._name == name:
            return True
        else:
            return False
