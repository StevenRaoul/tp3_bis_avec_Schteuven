from tp2 import *
def test_box_create():
    b = Box()

def test_box_add():
    b=Box()
    b.add("truc1")
    b.add("truc2")

def test_box_in():
    b= Box()
    b.add("chameau")
    b.add("al'ez")
    b.add("AAAAAAAAAAAAAAA")
    assert "chameau" in b
    assert "al'ez" in b
    assert "AAAAAAAAAAAAAAA" in b

def test_box_remove():
    b=Box()
    b.add("chameau")
    b.remove("chameau")
    assert "chameau" not in b

def test_box_openclose():
    b=Box()
    b.open()
    assert b.is_open()
    b.close()
    assert not b.is_open()

def test_box_look():
    b=Box()
    b.add("chameau")
    b.add("alez")
    assert b.action_look()=="la boite contient chameau, alez"
    b.close()
    assert b.action_look()=="la boite est fermée"

def creer_thing():
    t=Thing(3,"toto")
    assert t.volume()==3

def test_thing_volume():
    t=Thing(10,"toto")
    assert t.volume()==10

def capacity():
    b=Box()
    b.set_capacity(5)
    assert b.capacity() == 5

def test_boite_capacity():
    b=Box()
    assert b._capacity == None
    b._capacity = 5
    assert b._capacity == 5

def test_box_capacity():
    b=Box()
    objet=Thing(100000,"toto")
    assert b.has_room_for(objet)
    b.set_capacity(10000)
    assert not b.has_room_for(objet)

def test_action_add():
    b=Box()
    b.set_capacity(100)
    objet1 = Thing(30,"toto")
    objet2 = Thing(120,"toto")
    assert b.has_room_for(objet1)
    b.action_add(objet1)
    assert not b.has_room_for(objet2)

def creer_name_name():
    objet = Thing(45, "table basse")
    assert b.has_name("table basse")
    assert not b.has_name("table basse")
